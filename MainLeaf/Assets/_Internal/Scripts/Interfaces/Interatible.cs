using UnityEngine;
public interface Iinteratibles
{
    public string ActionName { get; }
    public bool CanInteract { get; }
    void OnDragObject();
    void OffDragObject();
    void MoveObject(Vector3 axis, GrabController grabcontroller);
}