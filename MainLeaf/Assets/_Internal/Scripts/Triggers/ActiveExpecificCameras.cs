using UnityEngine;
using Cinemachine;
public class ActiveExpecificCameras : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _camera;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out MovementController t))
        {
            _camera.enabled = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent(out MovementController t))
        {
            _camera.enabled = false;
        }
    }
}
