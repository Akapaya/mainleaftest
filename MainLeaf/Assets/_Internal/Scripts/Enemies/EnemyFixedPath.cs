﻿using UnityEngine;
using UnityEngine.AI;
using System.Threading.Tasks;
public class EnemyFixedPath : MonoBehaviour
{
    [SerializeField] private Vector3[] _positions;
    [SerializeField] private int _indexPositins = 0;
    [SerializeField] private int _timeToUpdateIndex = 4000;
    private NavMeshAgent _navMeshAgent;
    private Animator _animator;
    public delegate void OffNavMeshHandler();
    public static OffNavMeshHandler OffNavMeshEvent;

    private void OnEnable()
    {
        OffNavMeshEvent += OffNavMesh;
    }
    private void OnDisable()
    {
        OffNavMeshEvent -= OffNavMesh;
    }

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        UpdateIndex();
        UpdateDestination();
    }
    private void Update()
    {
        if(transform.position.CheckingApproximately(_positions[_indexPositins],2f))
        {
            _navMeshAgent.isStopped = true;
            UpdateIndex();
            UpdateDestination();
        }
    }

    private void OffNavMesh()
    {
        _navMeshAgent.isStopped = true;
    }
    private async void UpdateDestination()
    {
        AnimationManager.ChangeAnimationStatus(_animator, AnimationManager.enumAnimations.Movement, false);
        await Task.Delay(_timeToUpdateIndex);
        if (!_navMeshAgent)
        {
            return;
        }
        _navMeshAgent.isStopped = false;
        _navMeshAgent.SetDestination(_positions[_indexPositins]);
        AnimationManager.ChangeAnimationStatus(_animator, AnimationManager.enumAnimations.Movement, true);
    }
    private void UpdateIndex()
    {
        _indexPositins++;
        if(_indexPositins >= _positions.Length)
        {
            _indexPositins = 0;
        }
    }
}
