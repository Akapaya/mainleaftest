using System.Collections;
using UnityEngine;
using System.Threading.Tasks;
public class ObjectDragable : MonoBehaviour, Iinteratibles
{
    [SerializeField] private Rigidbody _rigidBody;
    [SerializeField] private LayerMask _layerCheckIfCanMove;
    [SerializeField] private Vector3 _offSet;
    [SerializeField] private Vector3 _targetPosition;
    [SerializeField] private Vector3 _direction;
    [SerializeField] private bool _canInteract = true;
    [SerializeField] private bool _inMovement;
    [SerializeField] private int _moveBoxDistance = 2;
    [SerializeField] private Vector3 _objectivePosition;
    [SerializeField] private string _uIactionName;
    public string ActionName => _uIactionName;
    public bool CanInteract => _canInteract;
    public GrabController Graber { get; set; }

    public delegate void PuzzleSolvedHandler();
    public static PuzzleSolvedHandler PuzzleSolvedEvent;

    private void OnEnable()
    {
        PuzzleSolvedEvent += BoxPosition;
    }

    private void OnDisable()
    {
        PuzzleSolvedEvent -= BoxPosition;
    }

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    private void BoxPosition()
    {
            transform.position = _objectivePosition;
    }

    private bool CheckIfCanMove()
    {
        
        return Physics.Raycast(transform.position + _offSet + _direction, _direction, _moveBoxDistance, _layerCheckIfCanMove);
    }

    private IEnumerator MovingObject()
    {
        yield return null;
        if (_inMovement == true && !transform.position.CheckingApproximately(_targetPosition, 0.1f))
        {
            _rigidBody.constraints = RigidbodyConstraints.None;
            _rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
            transform.position = Vector3.MoveTowards(transform.position, _targetPosition, 0.02f);
            IEnumerator Moving = MovingObject();
            StartCoroutine(Moving);
        }
        else
        {
            _inMovement = false;
            _rigidBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), Mathf.RoundToInt(transform.position.z));
        }
    }

        
    public void MoveObject(Vector3 axis, GrabController grabController)
    {
        _direction = axis;
        if (!CheckIfCanMove())
        {
            _targetPosition = transform.position + (_direction);
            _targetPosition = new Vector3(Mathf.RoundToInt(_targetPosition.x), transform.position.y, Mathf.RoundToInt(_targetPosition.z));
            _inMovement = true;
            IEnumerator Moving = MovingObject();
            StartCoroutine(Moving);
            grabController.OnMoving();
        }
    }

    public void OnDragObject()
    {
        _rigidBody.constraints = RigidbodyConstraints.None;
        _rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    public void OffDragObject()
    {
        _rigidBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out Iinteratibles t))
        {
            OffDragObject();
            _canInteract = false;
            Graber.OffDragObject();
        }
    }
}
