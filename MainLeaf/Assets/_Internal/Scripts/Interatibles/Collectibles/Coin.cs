using UnityEngine;
using DG.Tweening;
public class Coin : MonoBehaviour
{
    [SerializeField] private int _value;
    private void Update()
    {
        transform.DORotate(Vector3.up * 5, 0.5f, RotateMode.LocalAxisAdd);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out MovementController t))
        {
            UIManager.AddCoinTextEvent?.Invoke(_value);
            Destroy(this.gameObject);
        }
    }
}
