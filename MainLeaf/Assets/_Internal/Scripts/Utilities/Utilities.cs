using UnityEngine;
public static class Utilities
{
    public static bool CheckingApproximately(this Vector3 objA, Vector3 objB, float maxDiference)
    {
        if (objA.x > objB.x - maxDiference && objA.x < objB.x + maxDiference && objA.z > objB.z - maxDiference && objA.z < objB.z + maxDiference)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
