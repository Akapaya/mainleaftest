using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] Save _save;
    [SerializeField] private int _puzzleLevel;

    public int PuzzleLevel
    {
        get
        {
            return _puzzleLevel;
        }
        set
        {
            _puzzleLevel = value;
        }
    }
    public delegate void AddPuzzleLevelHandler(int scene);
    public static AddPuzzleLevelHandler AddPuzzleLevelEvent;
    public delegate void ResetSaveHandler();
    public static ResetSaveHandler ResetSaveEvent;

    void Start()
    {
        _puzzleLevel = _save.Level;
        if(SceneManager.GetActiveScene().buildIndex < PuzzleLevel)
        {
            ObjectDragable.PuzzleSolvedEvent?.Invoke();
        }
    }
    private void OnEnable()
    {
        AddPuzzleLevelEvent += AddLevel;
        ResetSaveEvent += ResetSave;
    }
    private void OnDisable()
    {
        AddPuzzleLevelEvent -= AddLevel;
        ResetSaveEvent -= ResetSave;
    }
    private void AddLevel(int scene)
    {
        if (_save.Level < scene)
        {
            _save.Level = scene;
        }
    }

    public void ResetSave()
    {
        _save.Level = 1;
    }
}
