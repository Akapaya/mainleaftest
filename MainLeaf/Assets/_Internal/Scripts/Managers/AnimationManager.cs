using UnityEngine;

public static class AnimationManager
{
    public enum enumAnimations { Movement, Jump, Crouch, Grab };

    public static void ChangeAnimationStatus(Animator animator, enumAnimations animation, bool status)
    {
        animator.SetBool(animation.ToString(), status);
    }
}
