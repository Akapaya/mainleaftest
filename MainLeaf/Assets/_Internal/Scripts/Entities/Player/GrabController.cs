using System.Collections;
using UnityEngine;
using Cinemachine;
using System.Threading.Tasks;
public class GrabController : MonoBehaviour, Iinteractor
{
    [SerializeField] private Camera _camera;
    [SerializeField] private Transform _lookDirection;
    [SerializeField] private Vector3 _targetMove;
    [SerializeField] private GameObject _targetObject;
    [SerializeField] private Vector3 _direction;
    [SerializeField] private Vector3 _playerPosition;
    [SerializeField] private CinemachineFreeLook _cmNormal;
    [SerializeField] private CinemachineFreeLook _cmGrab;
    [SerializeField] private bool _inGrabMovement = false;

    private Rigidbody _rigidBody;
    private Animator _animator;

    public delegate void GrabMovementHandler(float axisZ);
    public static GrabMovementHandler GrabMovementEvent;

    private void OnEnable()
    {
        GrabMovementEvent += GrabMovement;
    }

    private void OnDisable()
    {
        GrabMovementEvent -= GrabMovement;
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        _rigidBody = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        CheckTargetLine();
        
    }

    private void CheckTargetLine()
    {
        RaycastHit ray;
        Physics.Raycast(transform.position, _lookDirection.position, out ray,1.7f);
        Debug.DrawLine(transform.position, _lookDirection.position, Color.green, 1.7f);
        if (ray.collider == null || !ray.collider.gameObject.TryGetComponent(out Iinteratibles t))
        {
            UIManager.ChangeActionTextEvent?.Invoke("");
            _animator.SetBool("Grab", false);
            if (_targetObject != null)
            {
                if (_targetObject.TryGetComponent(out Iinteratibles interatible))
                {
                    interatible.OffDragObject();
                    _targetObject = null;
                    _cmGrab.enabled = false;
                    _cmNormal.enabled = true;
                }
            }
            return;
        }
        else
        {
            if (ray.collider.gameObject.TryGetComponent(out Iinteratibles interatible))
            {
                UIManager.ChangeActionTextEvent?.Invoke(interatible.ActionName);
                if (Input.GetMouseButton(0))
                {
                    if (_targetObject == null)
                    {
                        if(interatible.CanInteract)
                        MovementController.ChangeStatusEvent?.Invoke(MovementController._status.Wait);
                        _targetObject = ray.collider.gameObject;
                        _targetObject.GetComponent<ObjectDragable>().Graber = this;
                        interatible.OnDragObject();
                        _rigidBody.velocity = Vector3.zero;
                        _rigidBody.rotation = Quaternion.LookRotation(ray.normal * -1);
                        _rigidBody.freezeRotation = true;                 
                        _animator.SetBool("Grab", true);
                        _cmGrab.enabled = true;
                        _cmNormal.enabled = false;
                        OnDragObject();
                        
                    }
                }
                else
                {
                    MovementController.ChangeStatusEvent?.Invoke(MovementController._status.Normal);
                    _animator.SetBool("Grab", false);
                    interatible.OffDragObject();
                    _targetObject = null;
                    _cmGrab.enabled = false;
                    _cmNormal.enabled = true;
                }
            }
        }
    }

    private void GrabMovement(float zAxis)
    {
        if (_inGrabMovement == false)
        {
            _direction = new Vector3(0, 0, Mathf.RoundToInt(zAxis));
            Vector3 cameraZAxis = new Vector3(_camera.transform.forward.x, 0, _camera.transform.forward.z) * (zAxis);
            //cameraZAxis = cameraZAxis.normalized;
            _targetObject.GetComponent<ObjectDragable>().MoveObject(cameraZAxis, this);
            _targetMove = transform.position + (cameraZAxis);
            //_targetMove = new Vector3(Mathf.RoundToInt(_targetMove.x), transform.position.y, Mathf.RoundToInt(_targetMove.z));
        }
    }

    private IEnumerator MovingWithObject()
    {
        yield return null;
        if (_inGrabMovement == true && !transform.position.CheckingApproximately(_targetMove, 0.1f))
        {
            transform.position = Vector3.MoveTowards(transform.position, _targetMove, 0.02f);
            StartCoroutine("MovingWithObject");
        }
        else
        {
            _animator.SetBool("Movement", false);
            transform.position = _targetMove;
            yield return new WaitForSeconds(1.0f);
            _inGrabMovement = false;
        }
    }

    public void OnMoving()
    {
        StartCoroutine("MovingWithObject");
        _inGrabMovement = true;
        _animator.SetBool("Movement", true);
    }

    public void OffDragObject()
    {
        _animator.SetBool("Grab", false);
        
        MovementController.ChangeStatusEvent?.Invoke(MovementController._status.Normal);
        _targetObject = null;
        _cmGrab.enabled = false;
        _cmNormal.enabled = true;
        _cmNormal.transform.position = _cmGrab.transform.position;
    }

    public async void OnDragObject()
    {
            MovementController.ChangeStatusEvent?.Invoke(MovementController._status.Grab);
    }

    public void OffMoving()
    {
    }
}
