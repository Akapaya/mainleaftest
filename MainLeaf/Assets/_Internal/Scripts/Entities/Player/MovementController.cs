using UnityEngine;

public class MovementController : MonoBehaviour
{
    
    [SerializeField] private _status _enumStatus;
    [SerializeField] private Transform _camera;
    [SerializeField] private float _speedMovement = 1;
    [SerializeField] private float _jumpForce = 3;
    [SerializeField] private Vector3 _feet;
    [SerializeField] private LayerMask _groundLayersMask;
    [SerializeField] private Collider _normalCollider;
    [SerializeField] private Collider _crouchCollider;
    [SerializeField] public enum _status { Normal, Crouch, Grab, Wait };
    private float _axisX;
    private float _axisZ;
    private Rigidbody _rigidBody;
    private Animator _animator;
    public delegate void ChangeStatusHandler(_status enumStatus);
    public static ChangeStatusHandler ChangeStatusEvent;
    private void OnEnable()
    {
        ChangeStatusEvent += ChangeStatus;
    }
    private void OnDisable()
    {
        ChangeStatusEvent -= ChangeStatus;
    }
    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
        Time.timeScale = 1;
    }

    private void Update()
    {
        _axisX = Input.GetAxisRaw("Horizontal");
        _axisZ = Input.GetAxisRaw("Vertical");

        if (_axisX != 0 || _axisZ != 0)
        {
            AnimationManager.ChangeAnimationStatus(_animator,AnimationManager.enumAnimations.Movement,true);
            if (_enumStatus == _status.Grab)
            {
                GrabController.GrabMovementEvent?.Invoke(_axisZ);
            }
            else
            {
                Movement();
            }
        }
        else
        {
            _rigidBody.velocity = new Vector3(0, _rigidBody.velocity.y, 0);
            AnimationManager.ChangeAnimationStatus(_animator, AnimationManager.enumAnimations.Movement, false);
        }
        if (_enumStatus == _status.Normal)
        {
            if (CheckFeet())
            {
                if (Input.GetButtonDown("Jump")) { Jump(); }
            }
        }
        if (CheckFeet() && _enumStatus == _status.Normal || _enumStatus == _status.Crouch)
        {
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                CrouchAction();
            }
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            UIManager.PausePanelEvent?.Invoke();
        }
    }
    private void CrouchAction()
    {
        if (_enumStatus != _status.Crouch)
        {
            AnimationManager.ChangeAnimationStatus(_animator, AnimationManager.enumAnimations.Crouch, true);
            _normalCollider.enabled = false;
            _crouchCollider.enabled = true;
            ChangeStatus(_status.Crouch);
        }
        else
        {
            AnimationManager.ChangeAnimationStatus(_animator, AnimationManager.enumAnimations.Crouch, false);
            _normalCollider.enabled = true;
            _crouchCollider.enabled = false;
            ChangeStatus(_status.Normal);
        }
    }
    private bool CheckFeet()
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 1.3f);
        return hit.collider;
    }
    private void Movement()
    {
        Vector3 cameraXAxis = new Vector3(_camera.transform.right.x, 0, _camera.transform.right.z) * _axisX;
        Vector3 cameraZAxis = new Vector3(_camera.transform.forward.x, 0, _camera.transform.forward.z) * _axisZ;
        cameraXAxis = cameraXAxis.normalized;
        cameraZAxis = cameraZAxis.normalized;
        Vector3 axis = (cameraXAxis + cameraZAxis) * _speedMovement;
        _rigidBody.velocity = axis + new Vector3(0, _rigidBody.velocity.y, 0);
        Rotate();
    }
    private void Rotate()
    {
        Vector3 axisRotate = _rigidBody.velocity;
        axisRotate.y = 0;
        _rigidBody.rotation = Quaternion.LookRotation(axisRotate, Vector3.up);
    }
    private void Jump()
    {
        _rigidBody.AddForce(Vector2.up * _jumpForce, ForceMode.Impulse);
    }
    private void ChangeStatus(_status status)
    {
        _enumStatus = status;
    }
}
